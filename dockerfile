FROM openjdk:alpine
COPY . /opt
WORKDIR /opt/minecraft

RUN apk add --no-cache bash
RUN apk add --no-cache py-pip
RUN pip install mcstatus

RUN wget https://media.forgecdn.net/files/2982/548/Enigmatica5Server-0.5.1.zip
RUN unzip Enigmatica5Server-0.5.1.zip
RUN rm Enigmatica5Server-0.5.1.zip

RUN cp /opt/eula.txt /opt/minecraft

RUN chmod 700 server-start.sh

HEALTHCHECK --interval=30s --timeout=5s --retries=5 \
   CMD mcstatus localhost ping || exit 1

EXPOSE 25565
ENTRYPOINT ["/opt/minecraft/server-start.sh"]
